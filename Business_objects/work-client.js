const Dish = require('./src/dish');
const Menu = require('./src/menu');
const Order = require('./src/order');
const Product = require('./src/product');
const Rating = require('./src/rating');
const Waiter = require('./src/waiter');
const WaiterList = require('./src/waitersList');
const DB = require('./src/config/db');


class WorkClient {

    constructor() {
        this.db = new DB();
        this.waiters = [];
        this.waiterList = [];
        this.dish = this.db.getDish();
        this.menu = this.db.getMenu();
        this.order = this.db.getOrder();
        this.product = [];
        this.rating = null;
    }

// static addWaiter(name, rating, table) {
    //     db.addWaiter(name, rating, table);
    //     return db.getWaiter();
    // }
    //
    // static getWaiterBd() {
    //     return db.getWaiter();
    // }


    getMenu() {
        return this.menu;
    }

    async createMenu(menuId) {
        let res = await this.db.createMenu(menuId);
        // if (res)
        this.menu.push(new Menu(menuId, []));
        return res;
    }

    async addDishToMenu(menuId, dish) {
        let res = await this.db.addDishToMenu(menuId, dish);
        // if (res)
        for (let i = 0; i < this.menu.length; i++) {
            if (this.menu[i].name === menuId) {
                this.menu[i].listDish.push(dish);
                return true;
            }
        }
        return res;
    }

    getWaiter() {
        return this.db.getWaiter();
    }

    // getWaiterList() {
    //
    // }

    async addWaiter(name, rating, table) {
        return await this.db.addWaiter(name, rating, table);
    }

    // addWaiterToWaiterList(waiter) {
    //
    // }

    getDish() {
        return this.dish;
    }

    async addDish(name, products, price, photo, avalible) {
        let res = await this.db.addDish(name, products, price, photo, avalible);
        // if (res)
        this.dish.push(new Dish(name, products, price, photo, avalible));
        return res;
    }

    getProduct() {
        return this.db.getProduct();
    }

    async addProduct(name, price, photo) {
        let res = await this.db.addProduct(name, price, photo);
        // if (res)
        this.product.push(new Product(name, price, photo));
        return res;
    }

    getOrder() {
        return this.order;
    }

    async addOrder(listDish, waiter, table) {
        let res = await this.db.addOrder(listDish, waiter, table);
        // if (res)
        this.order.push(new Order(listDish, waiter, table));
        return res;
    }

    // getRating() {
    //
    // }
    //
    // addCriterion(name) {
    //
    // }
}


module.exports = WorkClient;
