const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const logger = require('log4js').getLogger('app');
const WorkClient = require('./work-client');
let workClient = new WorkClient();
const port = 8000;


app.use(bodyParser.json({limit: '100MB', type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true, limit: '100MB'}));
app.use(cors());

const asyncMiddleware = fn =>
    (req, res, next) => {
        // logger.debug('asyncMiddleware');
        Promise.resolve(fn(req, res, next))
            .catch(e => {
                logger.error('asyncMiddleware', e);
                res.status(500).json(e.message);
                next();
            });
    };

const appRouter = (app) => {

    app.get('/', (req, res) => {
        res.status(200).send('Welcome to Restaurant REST server');
    });
    // Return list of all menus
    app.get('/Menu', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getMenu());
    }));
    //
    // let cpUpload = upload.fields([{ name: 'file', maxCount: 1 }, { name: 'channelId', maxCount: 1 },{ name: 'targets'}]);
    //
    // Add new menu
    app.post('/Menu', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.createMenu(req.body.menuId));
    }));
    // Add dish or dishes in menu
    app.post('/Menu/:menuId/Dish', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addDishToMenu(req.params.menuId, req.body.dish));
    }));
    // Return all waiters
    app.get('/Waiter', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getWaiter());
    }));
    // Return waiters list, which made administrator
    app.get('/WaiterList', asyncMiddleware(async (req, res, next) => {
        res.json(await WorkClient.getWaiterList());
    }));
    // Add waiter
    app.post('/Waiter', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addWaiter(req.body.name, req.body.rating, req.body.table));
    }));
    // Add waiter to WaiterList
    app.post('/Waiter/WaiterList', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addWaiterToWaiterList(req.body.waiter));
    }));
    // Return all dishes
    app.get('/Dish', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getDish());
    }));
    // Add dish
    app.post('/Dish', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addDish(req.body.name, req.body.products, req.body.price, req.body.photo, req.body.avalible));
    }));
    // Return all products from db
    app.get('/Product', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getProduct());
    }));
    // Add product
    app.post('/Product', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addProduct(req.body.name, req.body.price, req.body.photo));
    }));

    app.get('/Product/:dish', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getProduct(req.params.menuId));
    }));
    // Return all orders
    app.get('/Order', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getOrder());
    }));

    app.post('/Order', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addOrder(req.body.listDish, req.body.waiter, req.body.table));
    }));
    // Return rating
    app.get('/Rating', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.getRating());
    }));
    // Add criterion into Rating
    app.post('/Rating', asyncMiddleware(async (req, res, next) => {
        res.json(await workClient.addCriterion(req.body.name));
    }));
    /*// Set criterion
    app.put('/Waiter/:waiterId/Rating', asyncMiddleware(async (req, res, next) => {
        res.json(await WorkClient.addWaiter(req.body.name, req.body.rating, req.body.table));
    }));*/

};

appRouter(app);

// mongoose.connect('mongodb://localhost/Restaurant', {useNewUrlParser: true}, function (err, db) {
//     if (err) throw err;
//     console.log('Successfully connected to ' + db.db.s.databaseName);
// });

app.listen(port, () => {
    console.log('We are live on ' + port);
});
