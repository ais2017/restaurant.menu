let assert = require("assert");
const WorkClient = require('../work-client');
let workClient = new WorkClient();

describe('Check all', () => {
    let menuName = 'Летнее';
    let products = {
        name: 'Сельдерей',
        price: 10,
        photo: 'img',
    };
    let dish = {
        name: 'Пачка сельдерея',
        product: products,
        price: 10,
        photo: 'img',
        avalible: true
    };
    let listDish = [dish];
    describe("Create menu, get menu", function () {
        it("Create", async function () {
            assert.equal(await workClient.createMenu(menuName), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getMenu()[0].name, menuName);
        });
    });

    describe("Create product, get product", function () {
        it("Create", async function () {
            assert.equal(await workClient.addProduct(products.name, products.price, products.photo), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getProduct()[0].name, products.name);
        });
    });

    describe("Create dish, get dish", function () {
        it("Create", async function () {
            assert.equal(await workClient.addDish(dish.name, dish.product, dish.price, dish.photo, dish.avalible), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getDish()[0].name, dish.name);
        });
    });

    describe("Add dish to menu", function () {
        it("Create", async function () {
            assert.equal(await workClient.addDishToMenu(menuName, dish), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getMenu()[0].listDish[0].name, 'Пачка сельдерея');
        });
    });

    describe("Create order, get order", function () {
        it("Create", async function () {
            assert.equal(await workClient.addOrder(listDish, 'oleg', 10), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getOrder()[0].waiter, 'oleg');
        });
    });

    describe("Create waiter, get waiter", function () {
        it("Create", async function () {
            assert.equal(await workClient.addWaiter('oleg', 10, 10), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getWaiter()[0].name, 'oleg');
        });
    });

    describe("add rating, get rating", function () {
        it("Create", async function () {
            assert.equal(await workClient.addWaiter('oleg', 10, 10), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getWaiter()[0].name, 'oleg');
        });
    });

});




