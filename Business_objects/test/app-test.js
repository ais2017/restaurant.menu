let assert = require("assert");
const Waiter = require('../src/waiter');
const Dish = require('../src/dish');
const Menu = require('../src/menu');
const WaitersList = require('../src/waitersList');
const Rating = require('../src/rating');
const Product = require('../src/product');
const Order = require('../src/order');
const WorkClient = require('../work-client');
let workClient = new WorkClient();


describe('Check dish.js, product.js and menu.js', () => {
    let prod = new Product('перец', 5, 'img');
    let prod1 = new Product('сало', 99999999, 'img');
    let prod2 = new Product('огурец', 2, 'img');
    let prod3 = new Product('жмых', 1, 'img');
    describe("product.js", function () {
        it("get product", function () {
            assert.equal(prod1.name, 'сало');
            assert.equal(prod._price, 5);
            assert.equal(prod.photo, 'img');
        });

        it("set product", function () {
            prod.name = 'морковь';
            prod.price = 3;
            prod.photo = 'newImg';
            assert.equal(prod.name, 'морковь');
            assert.equal(prod.price, 3);
            assert.equal(prod.photo, 'newImg');
        });
        prod.price = 5;
        prod.photo = 'img';
    });

    let arrProd = [prod, prod1, prod2, prod3];
    let secProd = [prod3, prod1];

    let dish1 = new Dish('plov', secProd, 100, 'img', true);
    let dish2 = new Dish('суп', arrProd, 'img', false);
    describe("dish.js", function () {
        it("get dish", function () {
            assert.equal(dish1.name, 'plov');
            assert.equal(dish1.price, 100);
            assert.equal(dish1.photo, 'img');
            assert.equal(dish1.products, secProd);
            assert.equal(dish1.avalible, true);

        });

        it("set dish", function () {
            dish1.avalible = false;
            dish1.products = arrProd;
            dish1.photo = 'newImg';
            dish1.price = 120;
            dish1.name = 'plov v2';
            assert.equal(dish1.name, 'plov v2');
            assert.equal(dish1.price, 120);
            assert.equal(dish1.photo, 'newImg');
            assert.equal(dish1.products, arrProd);
            assert.equal(dish1.avalible, false);
        });

        it("delprod", function () {
            assert.equal(dish1.delProduct(prod), true);
        });
        it("delprod with same", function () {
            assert.equal(dish1.delProduct(prod), false);
        });
        it("addprod with same", function () {
            assert.equal(dish1.addProduct(prod), true);
        });
        it("addprod with same", function () {
            assert.equal(dish1.addProduct(prod), false);
        });
    });

    let arr = [dish1, dish2];

    describe("menu.js", function () {
        const menu = new Menu('лето', arr);
        it("get", function () {
            assert.equal(menu.name, "лето");
        });
        it("delDish", function () {
            assert.equal(menu.delDish(dish1), true);
        });
        it("delDish with same", function () {
            assert.equal(menu.delDish(dish1), false);
        });
        it("addDish", function () {
            assert.equal(menu.addDish(dish1), true);
        });
        it("addDish", function () {
            assert.equal(menu.addDish(dish1), false);
        });
        it("get Dishes", function () {
            assert.equal(menu.listDish, arr);
        });
        it("set Dishes", function () {
            menu.name = 'kek';
            assert.equal(menu.name, 'kek');
            menu.name = 'лето'
        });
    });
    let waiter = new Waiter('Oleg', 10, null);
    let waiter1 = new Waiter('Julia', 5, null);
    let order = new Order(arr, waiter, 12);
    describe("order.js", function () {
        it("get for oreder", function () {
            assert.equal(order.waiter.name, 'Oleg');
            assert.equal(order.table, 12);
            assert.equal(order.listDish, arr);
        });
        it("setWaiter", function () {
            order.waiter = waiter1;
            assert.equal(order.waiter.name, 'Julia');
        });
        it("delDish", function () {
            assert.equal(order.delDish(dish1), true);
        });
        it("delDish", function () {
            assert.equal(order.delDish(dish1), false);
        });
        it("addDish", function () {
            assert.equal(order.addDish(dish1), true);
        });
        it("addDish with same", function () {
            assert.equal(order.addDish(dish1), false);
        });
    });
});

describe('Check waiters.js, rating.js and waitersList.js', () => {
    let waiter = new Waiter('Oleg', 10, null);
    let waiter1 = new Waiter('Julia', 5, null);
    let waiter2 = new Waiter('Katy', 6, null);
    let waiter3 = new Waiter('Jon', 8, null);
    describe("waiter.js", function () {
        it("get for waiter", function () {
            assert.equal(waiter.name, 'Oleg');
            assert.equal(waiter.table, null);
        });
        it("addWaiter", function () {
            assert.equal(waiter.addTable(10), true);
        });
        it("addWaiter for same", function () {
            assert.equal(waiter.addTable(10), false);
        });
        it("addWaiter for same", function () {
            let wat = new Waiter('Oleg', 10, [12, 23]);
            assert.equal(wat.addTable(10), true);
        });
        it("delWaiter", function () {
            assert.equal(waiter.delTable(10), true);
        });
        it("delWaiter for same", function () {
            assert.equal(waiter.delTable(10), false);
        });
        it("setName", function () {
            waiter.name = 'kek';
            assert.equal(waiter.name, 'kek');
        });
        waiter.name = 'Oleg';
    });
    let arr = new Map();
    arr.set('Speed', 0);
    arr.set('Aim', 0);
    arr.set('Agility', 0);
    arr.set('Dodge', 0);
    let rating = new Rating(arr);
    let rating1 = new Rating();
    describe("rating.js", function () {
        it("add criterions", function () {
            assert.equal(rating.listCriterion, arr);
            rating.addCriterion('kek');
            assert.equal(rating.getCriterionKey('Speed'), 'Speed');
            assert.equal(rating.getCriterionKey('kek'), 'kek');

        });
        it("add marks", function () {
            rating.addMark('Speed', 7);
            rating.addMark('Aim', 4);
            rating.addMark('Agility', 10);
            rating.addMark('Dodge', 2);
            assert.equal(rating.getCriterionValue('Speed'), 7);
        });
        it("delCriterion", function () {
            assert.equal(rating.delCriterion('Speed'), true);
        });
        it("delCriterion with same", function () {
            assert.equal(rating.delCriterion('Speed'), false);
        });
        it("setMark", function () {
            rating.setMark('Dodge', 5);
            assert.equal(rating.getCriterionValue('Dodge'), 5);
        });
        it("calculateRating", function () {
            assert.equal(rating.calculateRating(), 4.75);
        });

    });

    describe("rating.js and waiter.js", function () {
        waiter.rating = rating;
        it("get rating", function () {
            assert.equal(waiter.rating.calculateRating(), 4.75);
        });
    });

    let list = [waiter, waiter1, waiter2];
    let list1 = [waiter, waiter1];
    describe("waiterList.js", function () {
        let waitersList = new WaitersList(list);
        it("get for waiterList", function () {
            assert.equal(waitersList.listWaiter, list);
        });
        it("addWaiter", function () {
            assert.equal(waitersList.addWaiter(waiter3), true);
        });
        it("addWaiter with same", function () {
            assert.equal(waitersList.addWaiter(waiter3), false);
        });
        it("delWaiter", function () {
            assert.equal(waitersList.delWaiter(waiter3), true);
        });
        it("delWaiter", function () {
            assert.equal(waitersList.delWaiter(waiter3), false);
        });
        let sortList = list.sort(function (a, b) {
            return a.rating - b.rating
        });
        waitersList.sortWaiter();
        it("sortWaiters by ratings", function () {
            assert.equal(waitersList.listWaiter, sortList);
        });
        it("setWaiters ", function () {
            waitersList.listWaiter = list1;
            assert.equal(waitersList.listWaiter, list1);
        });
    });

});

describe("DB", function () {
    describe("Create menu, get menu", function () {
        it("Create", async function () {
            assert.equal(await workClient.createMenu('Летнее'), true);
        });
        it("Get", async function () {
            assert.equal(await workClient.getMenu()[0].name, 'Летнее');
        });
    });

    describe('Check all', () => {
        let menuName = 'Летнее';
        let products = {
            name: 'Сельдерей',
            price: 10,
            photo: 'img',
        };
        let dish = {
            name: 'Пачка сельдерея',
            product: products,
            price: 10,
            photo: 'img',
            avalible: true
        };
        let listDish = [dish];
        describe("Create menu, get menu", function () {
            it("Create", async function () {
                assert.equal(await workClient.createMenu(menuName), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getMenu()[0].name, menuName);
            });
        });

        describe("Create product, get product", function () {
            it("Create", async function () {
                assert.equal(await workClient.addProduct(products.name, products.price, products.photo), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getProduct()[0].name, products.name);
            });
        });

        describe("Create dish, get dish", function () {
            it("Create", async function () {
                assert.equal(await workClient.addDish(dish.name, dish.product, dish.price, dish.photo, dish.avalible), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getDish()[0].name, dish.name);
            });
        });

        describe("Add dish to menu", function () {
            it("Create", async function () {
                assert.equal(await workClient.addDishToMenu(menuName, dish), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getMenu()[0].listDish[0].name, 'Пачка сельдерея');
            });
        });

        describe("Create order, get order", function () {
            it("Create", async function () {
                assert.equal(await workClient.addOrder(listDish, 'oleg', 10), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getOrder()[0].waiter, 'oleg');
            });
        });

        describe("Create waiter, get waiter", function () {
            it("Create", async function () {
                assert.equal(await workClient.addWaiter('oleg', 10, 10), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getWaiter()[0].name, 'oleg');
            });
        });

        describe("add rating, get rating", function () {
            it("Create", async function () {
                assert.equal(await workClient.addWaiter('oleg', 10, 10), true);
            });
            it("Get", async function () {
                assert.equal(await workClient.getWaiter()[0].name, 'oleg');
            });
        });

    });
});


