const Dish = require('../dish');
const Menu = require('../menu');
const Order = require('../order');
const Product = require('../product');
const Rating = require('../rating');
const Waiter = require('../waiter');
const WaiterList = require('../waitersList');

class DB {

    constructor() {
        this.waiters = [];
        this.waiterList = [];
        this.dish = [];
        this.menu = [];
        this.order = [];
        this.product = [];
        this.rating = null;
    }

    getMenu() {
        return this.menu;
    }

    async createMenu(menuId) {
        try {
            await this.menu.push(new Menu(menuId, []));
            return true
        }
        catch (e) {
            return false;
        }
    }

    addWaiter(name, rating, table) {
        try {
            this.waiters.push(new Waiter(name, rating, table));
            return true;
        } catch
            (e) {
            return false;
        }
    };

    getWaiter() {
        return this.waiters;
    }


    addDishToMenu(menuId, dish) {
        try {
            for (let i = 0; i < this.menu.length; i++) {
                if (this.menu[i].name === menuId) {
                    this.menu[i].listDish.push(dish);
                    return true;
                }
            }
        } catch
            (e) {
            return false;
        }
    }

    addDish(name, products, price, photo, avalible) {
        try {
            this.dish.push(new Dish(name, products, price, photo, avalible));
            return true;
        } catch
            (e) {
            return false;
        }
    }

    addProduct(name, price, photo) {
        try {
            this.product.push(new Product(name, price, photo));
            return true;
        }
        catch
            (e) {
            return false;
        }
    }

    getProduct() {
        return this.product;
    }

    getDish() {
        return this.dish;
    }

    addOrder(listDish, waiter, table) {
        try {
            this.order.push(new Order(listDish, waiter, table));
            return true;
        }
        catch
            (e) {
            return false;
        }
    }

    getOrder() {
        return this.order;
    }
}

module.exports = DB;
