const mongoose = require('mongoose');

let clientSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    listDish: [Number]
});

let Menu = mongoose.model('Menu', clientSchema);

module.exports = Menu;
