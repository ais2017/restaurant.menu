const mongoose = require('mongoose');
const Menu = require('./menu');
const Waiter = require('./waiter');
const ObjectID = require('mongodb').ObjectID;

class Route {
    static addWaiter(name, rating, table) {
        let newWaiter = new Waiter({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            rating: rating,
            table: table
        });

        newWaiter.save(function (err, waiter) {
            if (err) throw err;
            console.log(waiter);
            console.log('Client successfully saved.');
        });
    };

    static async getWaiter() {
        return Waiter.find().exec();
    }


}

module.exports = Route;


