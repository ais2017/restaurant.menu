const mongoose = require('mongoose');

let clientSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    rating: Number,
    // created: {
    //     type: Date,
    //     default: Date.now
    // },
    table: Number
});

let Waiter = mongoose.model('Waiter', clientSchema);

module.exports = Waiter;
