const _ = require('lodash');

class Waiter {

    constructor(name, rating, table) {

        this._name = name;
        this._rating = rating;
        this._table = table;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get rating() {
        return this._rating;
    }

    set rating(value) {
        this._rating = value;
    }

    get table() {
        return this._table;
    }

    addTable(table) {
        if (_.isNil(_.find(this._table, function (o) {
            return o === table;
        }))) {
            if(_.isNil(this._table))
                this._table = [];
            this._table.push(table);
            return true;
        } else
            return false;

    }

    delTable(table) {
        if (!_.isNil(_.find(this._table, function (o) {
            return o === table;
        }))) {
            _.remove(this._table, function (o) {
                return table === table;
            });
            return true;
        } else
            return false;
    }

}

module.exports = Waiter;
