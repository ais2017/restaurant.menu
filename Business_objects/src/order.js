const _ = require('lodash');

class Order {

    constructor(listDish, waiter, table) {
        this._listDish = listDish;
        this._waiter = waiter;
        this._table = table;
    }


    get listDish() {
        return this._listDish;
    }

    // set listDish(value) {
    //     this._listDish = value;
    // }

    get waiter() {
        return this._waiter;
    }

    set waiter(value) {
        this._waiter = value;
    }

    get table() {
        return this._table;
    }

    // set table(value) {
    //     this._table = value;
    // }

    addDish(dish){
        if (_.isNil(_.find(this._listDish, function (o) {
            return o === dish;
        }))) {
            this._listDish.push(dish);
            return true;
        } else
            return false;
    }

    delDish(dish){
        if (!_.isNil(_.find(this._listDish, function (o) {
            return o === dish;
        }))) {
            _.remove(this._listDish, function(o) {
                return o.name === dish.name;
            });
            return true;
        } else
            return false;
    }
}

module.exports = Order;
