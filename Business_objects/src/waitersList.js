const _ = require('lodash');

class WaitersList {

    constructor(listWaiter) {
        this._listWaiter = listWaiter;
    }

    get listWaiter() {
        return this._listWaiter;
    }

    set listWaiter(value) {
        this._listWaiter = value;
    }

    addWaiter(waiter){
        if (_.isNil(_.find(this._listWaiter, function (o) {
            return o === waiter;
        }))) {
            this._listWaiter.push(waiter);
            return true;
        } else
            return false;
    }

    delWaiter(waiter){
        if (!_.isNil(_.find(this._listWaiter, function (o) {
            return o === waiter;
        }))) {
            _.remove(this._listWaiter, function(o) {
                return o.name === waiter.name;
            });
            return true;
        } else
            return false;
    }

    sortWaiter(){
       this._listWaiter = this._listWaiter.sort(function (a, b) {
            return a.rating - b.rating
        });
    }
}

module.exports = WaitersList;
