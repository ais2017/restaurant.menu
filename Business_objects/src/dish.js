const _ = require('lodash');

class Dish {

    constructor(name, products, price, photo, avalible) {
        this._name = name;
        this._products = products;
        this._price = price;
        this._photo = photo;
        this._avalible = avalible;
    }

    get name() {
        return this._name;
    }

    get products() {
        return this._products;
    }

    get price() {
        return this._price;
    }

    set price(value) {
        this._price = value;
    }

    get photo() {
        return this._photo;
    }

    set photo(value) {
        this._photo = value;
    }

    get avalible() {
        return this._avalible;
    }

    set avalible(value) {
        this._avalible = value;
    }


    set name(value) {
        this._name = value;
    }

    set products(value) {
        this._products = value;
    }

    addProduct(prod){
        if (_.isNil(_.find(this.products, function (o) {
            return o === prod;
        }))) {
            this.products.push(prod);
            return true;
        } else
            return false;
    }

    delProduct(prod){
        if (!_.isNil(_.find(this.products, function (o) {
            return o === prod;
        }))) {
            _.remove(this.products, function(o) {
                return o.name === prod.name;
            });
            return true;
        } else
            return false;
    }
}

module.exports = Dish;
