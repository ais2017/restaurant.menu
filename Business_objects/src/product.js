class Product {

    constructor(name, price, photo) {

        this._name = name;
        this._price = price;
        this._photo = photo;
    }


    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get price() {
        return this._price;
    }

    set price(value) {
        this._price = value;
    }

    get photo() {
        return this._photo;
    }

    set photo(value) {
        this._photo = value;
    }
}

module.exports = Product;
