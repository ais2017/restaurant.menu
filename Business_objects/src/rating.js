class Rating {

    constructor(listCriterion) {
        this._listCriterion = listCriterion || new Map();
    }


    get listCriterion() {
        return this._listCriterion;
    }

    // set listCriterion(value) {
    //     this._listCriterion = value;
    // }

    getCriterionValue(criterion) {
        for (let [key, value] of this._listCriterion.entries()) {
            if (key === criterion)
                return value;
        }
    }

    getCriterionKey(criterion) {
        for (let [key, value] of this._listCriterion.entries()) {
            if (key === criterion)
                return key;
        }
    }


    calculateRating() {
        let rating = 0;
        this._listCriterion.forEach((value, key) => {
            rating += value;
        });
        return rating / this._listCriterion.size;
    }

    addCriterion(criterion) {
        return this._listCriterion.set(criterion, 0);
    }

    delCriterion(criterion) {
        return this._listCriterion.delete(criterion)
    }

    addMark(criterion, mark) {
        this._listCriterion.set(criterion, mark);
    }

    setMark(criterion, mark) {
        this._listCriterion.set(criterion, mark);
    }

}

module.exports = Rating;
