const _ = require('lodash');

class Menu {

    constructor(name, listDish) {
        this._name = name;
        this._listDish = listDish || [];
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get listDish() {
        return this._listDish;
    }

    // set listDish(value) {
    //     this._listDish = value;
    // }

    addDish(prod) {
        if (_.isNil(_.find(this.listDish, function (o) {
            return o === prod;
        }))) {
            this.listDish.push(prod);
            return true;
        } else
            return false;
    }

    delDish(prod) {
        if (!_.isNil(_.find(this.listDish, function (o) {
            return o === prod;
        }))) {
            _.remove(this.listDish, function (o) {
                return o.name === prod.name;
            });
            return true;
        } else
            return false;
    }

}

module.exports = Menu;
